﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {
	public SpriteRenderer sr;
	public Transform tf;
	public float speed;

	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Space)) {
			sr.color = Color.blue;
		} else {
			sr.color = Color.white;
		}

		if (Input.GetKey (KeyCode.W)) {
			// Move to the right
			tf.position = tf.position + Vector3.up;
		}
		if (Input.GetKey (KeyCode.D)) {
			// Move to the right
			tf.position = tf.position + Vector3.right;
		}
		if (Input.GetKey (KeyCode.A)) {
			// Move to the right
			tf.position = tf.position + Vector3.left;
		}
		if (Input.GetKey (KeyCode.S)) {
			// Move to the right
			tf.position = tf.position + Vector3.down;
		}

	}
}