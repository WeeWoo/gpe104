﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
	public Transform tf;
	public float speed;
	// Use this for initialization
	void Start () 	{
		tf = GetComponent<Transform> (); //calls transfrom component
		gameObject.SetActive (true);//sets game object to true 
	}

	// Update is called once per frame
	void Update ()
	{
		tf.position = tf.position + Vector3.zero;// Puts sprite in center of world 
		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.RightArrow)) {
			tf.position = tf.position + Vector3.right * .05f; //Moves Sprite one unit right
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			tf.position = tf.position + Vector3.right;//moves sprite right
		}
		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.LeftArrow)) {
			tf.position = tf.position + Vector3.left * .05f; //Moves Sprite one unit left
		} else if (Input.GetKey (KeyCode.LeftArrow)) {
			tf.position = tf.position + Vector3.left;//moves sprite left
		}
		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.DownArrow)) {
			tf.position = tf.position + Vector3.down * .05f; //Moves Sprite one unit down
		} else if (Input.GetKey (KeyCode.DownArrow )) {
			tf.position = tf.position + Vector3.down;//moves sprite down
		}
		if (Input.GetKey (KeyCode.Space)) {
			tf.position = Vector3.zero; //returns sprite to center of the world 
		}
		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.UpArrow)) {
			tf.position = tf.position + Vector3.up * .05f; //Moves Sprite one unit 
		} else if (Input.GetKey (KeyCode.UpArrow)) {
			tf.position = tf.position + Vector3.up; //moves sprite up
		}
		if (Input.GetKeyDown (KeyCode.Q)) {
			gameObject.SetActive (false); //sets game object to off
		}
		if (Input.GetKey (KeyCode.P)) {
			//code goes here but have no idea how to do this, looked at lecture 3.2 no examples. 
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();//cleanly extis the app 
		}
	}
}                   